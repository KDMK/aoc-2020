const fs = require("fs");

const pairs = {};
let numbers = fs.readFileSync("./advent-of-code-2020-1-input-1.txt", "utf-8").split("\n").map(m => Number.parseInt(m));

numbersMap = {}
remainders = {};

numbersMap[numbers[0]] = true;
for(let i = 0; i < numbers.length; i++) {
    for(let j = i + 1; j < numbers.length; j++) {
        if (!numbersMap[numbers[j]]) {
            numbersMap[numbers[j]] = true;
        }
        let remainder = 2020 - numbers[i] - numbers[j];
        remainders[remainder] = {number1: numbers[i], number2: numbers[j]};
        if(numbersMap[remainder]) {
            console.log(remainder);
            console.log(remainders[remainder]);
        }
    }
}