const fs = require("fs");

const pairs = {};
const numbers = fs.readFileSync("./advent-of-code-2020-1-input-1.txt", "utf-8").split("\n");

let foundNumbers = [];
for(let number of numbers) {
    let counter = 2020 - number;
    if(typeof pairs[number] === "undefined") {
        pairs[counter] = { number: number, found: false };
    } else {
        pairs[number].found = true;
        foundNumbers.push(number, counter);
    }
}

console.log(foundNumbers[0] * foundNumbers[1]);