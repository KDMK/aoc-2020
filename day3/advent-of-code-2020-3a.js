const rfs = require("fs").readFileSync;

const data = rfs("./advent-of-code-2020-3-input-1.txt", "utf-8").split("\n")
  .map(line => line.split(""));

function traverseMap(map, stepDown, stepRight) {
    console.log("Step right: " + stepRight);
    console.log("Step down: " + stepDown);
    console.log();

    let trees = 0;
    let frees = 0;
    
    let j = 0;
    for(let i = 0; i < data.length; i += stepDown) {
      if(map[i][j] === '#') trees++;
      else if(map[i][j] === '.') frees++;
    
      j = (j+stepRight) % map[0].length;
    }

    console.log("frees: " + frees);
    console.log("trees: " + trees);
    console.log("######################################\n");

    return trees;
}

let numbers = [];
numbers.push(traverseMap(data, 1, 1));
numbers.push(traverseMap(data, 1, 3));
numbers.push(traverseMap(data, 1, 5));
numbers.push(traverseMap(data, 1, 7));
numbers.push(traverseMap(data, 2, 1));

console.log("trees multiplied: " + numbers.reduce((agg, cur) => agg * cur, 1));