const rfs = require("fs").readFileSync;

let seatingPlan = rfs("./advent-of-code-2020-11-input-1.txt", "utf-8")
    .split("\n")
    .map(line => line.split(""));

function checkEmptyNeighbors(row, col, floorPlan) {
    // special cases:
    let occupied = 0;

    let found_empty_1 = false;
    let found_empty_2 = false;
    let found_empty_3 = false;
    let found_empty_4 = false;
    let found_empty_5 = false;
    let found_empty_6 = false;
    let found_empty_7 = false;
    let found_empty_8 = false;

    let found_full_1 = false;
    let found_full_2 = false;
    let found_full_3 = false;
    let found_full_4 = false;
    let found_full_5 = false;
    let found_full_6 = false;
    let found_full_7 = false;
    let found_full_8 = false;

    let i = 1;
    while (true) {
        if (
            (row + i > floorPlan.length || row - i < 0) &&
            (col + i > floorPlan[0].length || col - i < 0)
        ) {
            break
        }

        // provjera gore
        if ((row - i + 1) > 0 && !(found_empty_1 || found_full_1)) {
            if (floorPlan[row - i][col] === "#") {
                found_full_1 = true;
                occupied++;
            } else if (floorPlan[row - i][col] === "L") {
                found_empty_1 = true;
            }
        }
        // provjera gore lijevo
        if (((col - i + 1) > 0 && (row - i + 1) > 0) && !(found_empty_2 || found_full_2)) {
            if (floorPlan[row - i][col - i] === '#') {
                found_full_2 = true;
                occupied++;
            } else if (floorPlan[row - i][col - i] === 'L') {
                found_empty_2 = true;
            }
        }
        // provjera gore desno
        if (((row - i + 1) > 0 && (col + i - 1) < floorPlan[0].length - 1) && !(found_empty_3 || found_full_3)) {
            if (floorPlan[row - i][col + i] === '#') {
                found_full_3 = true;
                occupied++;
            } else if (floorPlan[row - i][col + i] === 'L') {
                found_empty_3 = true;
            }
        }

        // provjera dolje
        if (((row + i - 1) < floorPlan.length - 1) && !(found_empty_4 || found_full_4)) {
            if (floorPlan[row + i][col] === '#') {
                found_empty_4 = true;
                occupied++;
            } else if (floorPlan[row + i][col] === 'L') {
                found_empty_4 = true;
            }
        }
        // provjera dolje lijevo
        if (((col - i + 1) > 0 && (row + i - 1) < floorPlan.length - 1) && !(found_empty_5 || found_full_5)) {
            if (floorPlan[row + i][col - i] === '#') {
                found_full_5 = true;
                occupied++;
            } else if (floorPlan[row + i][col - i] === 'L') {
                found_empty_5 = true;
            }
        }
        // provjera dolje desno
        if (((col + i - 1) < floorPlan[0].length - 1 && (row + i - 1) < floorPlan.length - 1) && !(found_empty_6 || found_full_6)) {
            if (floorPlan[row + i][col + i] === '#') {
                found_full_6 = true;
                occupied++;
            } else if (floorPlan[row + i][col + i] === 'L') {
                found_empty_6 = true;
            }
        }

        // provjera lijevo
        if ((col - i + 1) > 0 && !(found_empty_7 || found_full_7)) {
            if (floorPlan[row][col - i] === '#') {
                found_full_7 = true;
                occupied++;
            } else if (floorPlan[row][col - i] === 'L') {
                found_empty_7 = true;
            }
        }
        // provjera desno
        if ((col + i - 1) < floorPlan[0].length - 1 && !(found_empty_8 || found_full_8)) {
            if (floorPlan[row][col + i] === '#') {
                found_full_8 = true;
                occupied++;
            } else if (floorPlan[row][col + i] === 'L') {
                found_empty_8 = true;
            }
        }

        i++;
    }

    return occupied;
}


function processOneRoundOfSeating(floorPlan) {
    let newPlan = JSON.parse(JSON.stringify(floorPlan));

    let changed = false;
    for (let row = 0; row < floorPlan.length; row++) {
        for (let col = 0; col < floorPlan[0].length; col++) {
            if (floorPlan[row][col] === '.') {
                continue;
            }
            let calculatedNeighbors = checkEmptyNeighbors(row, col, floorPlan);
            // If a seat is empty (L) and there are no occupied seats adjacent to it, the seat becomes occupied.
            if (floorPlan[row][col] === 'L' && calculatedNeighbors === 0) {
                newPlan[row][col] = '#';
                changed = true;
                continue;
            }

            // If a seat is occupied (#) and four or more seats adjacent to it are also occupied, the seat becomes empty.
            if (floorPlan[row][col] === '#' && calculatedNeighbors >= 5) {
                newPlan[row][col] = 'L';
                changed = true;
                continue;
            }
        }
    }

    return { floorPlan: newPlan, changed: changed };
}

function calculateTakenSeats(floorPlan) {
    let total = 0;
    for (let i = 0; i < floorPlan.length; i++) {
        for (let j = 0; j < floorPlan[0].length; j++) {
            if (floorPlan[i][j] === '#') total++;
        }
    }

    return total;
}

iterations = 1;
let processed = processOneRoundOfSeating(seatingPlan);
while(processed.changed) {
    processed = processOneRoundOfSeating(processed.floorPlan);
    iterations++;
}



console.log(processed);
console.log(calculateTakenSeats(processed.floorPlan));