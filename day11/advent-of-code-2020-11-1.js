const rfs = require("fs").readFileSync;

let seatingPlan = rfs("./advent-of-code-2020-11-input-1.txt", "utf-8")
    .split("\n")
    .map(line => line.split(""));

function checkSeatNeighbors(row, col, floorPlan) {
    // special cases:
    let occupied = 0;
    // provjera gore
    if (row > 0 && floorPlan[row - 1][col] === "#") {
        occupied++;
    }
    // provjera gore lijevo
    if ((col > 0 && row > 0) && floorPlan[row - 1][col - 1] === '#') {
        occupied++;
    }
    // provjera gore desno
    if ((row > 0 && col < floorPlan[0].length - 1) && floorPlan[row - 1][col + 1] === '#') {
        occupied++;
    }

    // provjera dolje
    if ((row < floorPlan.length - 1) && floorPlan[row + 1][col] === '#') {
        occupied++;
    }
    // provjera dolje lijevo
    if ((col > 0 && row < floorPlan.length - 1) && floorPlan[row + 1][col - 1] === '#') {
        occupied++;
    }
    // provjera dolje desno
    if ((col < floorPlan[0].length - 1 && row < floorPlan.length - 1) && floorPlan[row + 1][col + 1] === '#') {
        occupied++;
    }

    // provjera desno
    if (col > 0 && floorPlan[row][col - 1] === '#') {
        occupied++;
    }
    // provjera lijevo
    if (col < floorPlan[0].length - 1 && floorPlan[row][col + 1] === '#') {
        occupied++;
    }

    return occupied;
}


function processOneRoundOfSeating(floorPlan) {
    let newPlan = JSON.parse(JSON.stringify(floorPlan));

    let changed = false;
    for (let row = 0; row < floorPlan.length; row++) {
        for (let col = 0; col < floorPlan[0].length; col++) {
            if (floorPlan[row][col] === '.') {
                continue;
            }
            let calculatedNeighbors = checkSeatNeighbors(row, col, floorPlan);
            // If a seat is empty (L) and there are no occupied seats adjacent to it, the seat becomes occupied.
            if(floorPlan[row][col] === 'L' && calculatedNeighbors === 0) {
                newPlan[row][col] = '#';
                changed = true;
                continue;
            }

            // If a seat is occupied (#) and four or more seats adjacent to it are also occupied, the seat becomes empty.
            if(floorPlan[row][col] === '#' && calculatedNeighbors >= 4) {
                newPlan[row][col] = 'L';
                changed = true;
                continue;
            }
        }
    }

    return {floorPlan: newPlan, changed: changed};
}

function calculateTakenSeats(floorPlan) {
    let total = 0;
    for(let i = 0; i < floorPlan.length; i++) {
        for(let j = 0; j < floorPlan[0].length; j++) {
            if (floorPlan[i][j] === '#') total++;
        }
    }

    return total;
}

iterations = 1;
let processed = processOneRoundOfSeating(seatingPlan);
while(processed.changed) {
    processed = processOneRoundOfSeating(processed.floorPlan);
    iterations++;
}



console.log(processed);
console.log(calculateTakenSeats(processed.floorPlan));