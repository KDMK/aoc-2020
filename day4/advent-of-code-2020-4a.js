const rfs = require("fs").readFileSync;

const validBitMask1 = 255;
const validBitMask2 = 127;

function bit_test(num, bit){
    return ((num>>bit) % 2 != 0)
}

function bit_set(num, bit){
    return num | 1<<bit;
}

function bit_clear(num, bit){
    return num & ~(1<<bit);
}

function isValid(validator) {
    return validator === validBitMask1 || validator === validBitMask2;
}

let data = rfs("./advent-of-code-2020-4-input-1.txt", "utf-8").split("\n");

function validationBitFrom(field) {
    switch(field) {
        case "byr":
            return 0;
        case "iyr":
            return 1;
        case "eyr":
            return 2;
        case "hgt":
            return 3;
        case "hcl":
            return 4;
        case "ecl":
            return 5;
        case "pid":
            return 6;
        case "cid":
            return 7;
    }
}

// final result was off by one? Some rule didnt catch edge case
function validationBitFromRules(key, value, person) {
    switch(key) {
        case "byr":
            if(value >= 1920 && value <= 2002) {
                person.validator = bit_set(person.validator, validationBitFrom(key));
            }
            break;
        case "iyr":
            if(value >= 2010 && value <= 2020) {
                person.validator = bit_set(person.validator, validationBitFrom(key));
            }
            break;
        case "eyr":
            if(value >= 2020 && value <= 2030) {
                person.validator = bit_set(person.validator, validationBitFrom(key));
            }
            break;
        case "hgt":
            if(value.includes("in")) {
                let realValue = Number.parseInt(value.replace(/in/g, ""));
                // ISSUE WAS HERE <----- some how "at most 76" doesn't mean 76 included?
                if(realValue >= 59 && realValue < 76) {
                    person.validator = bit_set(person.validator, validationBitFrom(key));
                }
            } else if (value.includes("cm")) {
                let realValue = Number.parseInt(value.replace(/cm/g, ""));
                if(realValue >= 150 && realValue <= 193) {
                    person.validator = bit_set(person.validator, validationBitFrom(key));
                }
            }
            break;
        case "hcl":
            if(/#[0-9a-f]{6,6}/g.test(value)) {
                person.validator = bit_set(person.validator, validationBitFrom(key));
            }
            break;
        case "ecl":
            if(/(amb|blu|brn|gry|grn|hzl|oth)/g.test(value)) {
                person.validator = bit_set(person.validator, validationBitFrom(key));
            }
            break;
        case "pid":
            if(/[0-9]{9,9}/g.test(value)) {
                person.validator = bit_set(person.validator, validationBitFrom(key));
            }
            break;
        case "cid":
            person.validator = bit_set(person.validator, validationBitFrom(key));
    }
}

function initializeNewPerson() {
    return {validator: 0};
}

function processInputLines(lines) {
    let passportsValid = [];
    let passportsInvalid = [];
    
    let person = {validator: 0};
    for(const line of lines) {
        if(line.trim() == 0) {
            if(isValid(person.validator)) {
                passportsValid.push(person);
                person = initializeNewPerson();
            } else {
                passportsInvalid.push(person);
                person = initializeNewPerson();
            }
            continue;
        }

        let splittedFields = line.split(" ");
        for(let field of splittedFields) {
            let [key,value] = field.split(":");
            person[key] = value;
            validationBitFromRules(key,value,person);
            // person.validator = bit_set(person.validator, validationBitFrom(key));
        }
    }

    return [passportsValid, passportsInvalid];
}

let retVal = processInputLines(data);

console.log(retVal[0].length);
console.log(retVal[1].length);