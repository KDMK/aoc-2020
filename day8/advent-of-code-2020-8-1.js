const rfs = require("fs").readFileSync;

const commands = rfs("./advent-of-code-2020-8-input-1.txt", "utf-8")
    .split("\n")
    .map((line) => {
        const splitted = line.split(" ");

        return {
            command: splitted[0],
            amount: Number.parseInt(splitted[1]),
            visited: false
        }
    });

// (##########################################################################)
function executeProgram(commands) {
    let acc = 0;
    let pc = 0;

    let curCommand = commands[pc];
    while (true) {
        curCommand = commands[pc];
        if (!curCommand) {
            console.log("Program finished execution!");
            console.log(`acc: ${acc}`);
            return true;
        }

        if (curCommand.visited) {
            console.log('Infinite loop detected!');
            console.log(`acc: ${acc}`);
            return false;
        }

        curCommand.visited = true;

        if (curCommand.command === 'nop') {
            pc++;
            continue;
        }

        if (curCommand.command === 'acc') {
            acc += curCommand.amount;
            pc++;
            continue;
        }

        if (curCommand.command === 'jmp') {
            if(pc + curCommand >= commands.length) {
                console.log("pc overflow");
                return false;
            }
            pc += curCommand.amount;
        }
    }
}

function findFaultyCommand(commands) {
    for(let i = 0; i < commands.length; i++) {
        let curCommand = commands[i];
        if (curCommand.command === 'jmp') {
            let changedCommands = JSON.parse(JSON.stringify(commands));
            changedCommands[i].command = 'nop';
            if (executeProgram(changedCommands)) {
                console.log("Found correct command!");
                console.log("Faulty commands is on index " + i);
                break;
            }
        }
        if (curCommand.command === 'nop') {
            let changedCommands = JSON.parse(JSON.stringify(commands));
            changedCommands[i].command = 'jmp';
            if (executeProgram(changedCommands)) {
                console.log("Found correct command!");
                console.log("Faulty commands is on index " + i);
                break;
            }
        }
    }
}

// executeProgram(commands);
findFaultyCommand(commands);