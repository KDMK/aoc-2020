const rfs = require("fs").readFileSync;

function processInput(input) {
    if (!input || input.trim().length === 0) {
        return []
    }

    let associatedValues = input.split("contain");
    let key = associatedValues[0].trim().replace("bags", "bag");
    let values = {};

    if (associatedValues[1].indexOf("no other") != -1) {
        return [{ bagName: key, contains: {} }];
    }
    let splittedValues = associatedValues[1].split(",");
    for (let i = 0; i < splittedValues.length; i++) {
        let curVal = splittedValues[i].trim();
        let numericValue = Number.parseInt(curVal.substring(0, 1));
        let textValue = curVal
            .substring(1)
            .trim()
            .replace(".", "");
        textValue = textValue.replace("bags", "bag");
        values[textValue] = numericValue;
    }

    return [{ bagName: key, contains: values }];
}

function reducer(agg, cur) {
    agg[cur.bagName] = cur.contains;
    return agg;
}

function locator(bag, curKey, source) {
    if (source[curKey] === {}) {
        return false;
    }
    let sourceKeys = Object.keys(source[curKey]);
    if(sourceKeys.indexOf(bag) != -1) {
        return true;
    }

    let found = false;
    for(let key of sourceKeys) {
        found = locator(bag, key, source);
        if (found) {
            break;
        }
    }
    
    return found;
}

function locatePossibleShinyGoldBags(bags) {
    let possibleBags = [];
    
    for(let bag of Object.keys(bags)) {
        if(locator("shiny gold bag", bag, bags)) {
            possibleBags.push(bag);
        };
    }

    return possibleBags;
}

function findAllBagsInsideShinyGoldBag(bags) {
    return findAllBagsInside(bags["shiny gold bag"], bags);
}

function findAllBagsInside(bag, bags) {
    console.log(bag);
    if (!bag || bag === {}) {
        return 0;
    }

    let total = Object.values(bag).reduce((a, b) => a + b, 0);
    for(let key of Object.keys(bag)) {
        console.log(key);
        total += (bag[key] * findAllBagsInside(bags[key], bags));
    }

    return total;
}

const data = rfs("./advent-of-code-2020-7-input-1.txt", "utf-8").split('\n');

let processedData = data
    .flatMap(processInput)
    .reduce(reducer, {});

console.log(locatePossibleShinyGoldBags(processedData).length);
console.log(Object.keys(processedData).length);

console.log(findAllBagsInsideShinyGoldBag(processedData));