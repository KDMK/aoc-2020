const rfs = require("fs").readFileSync;

let numbers = rfs("./advent-of-code-2020-10-input-1.txt", "utf-8")
    .split("\n")
    .map((line) => Number.parseInt(line));

numbers = numbers.sort((a, b) => { if (a > b) return 1; else if (a < b) return -1; else return 0 })
numbers.unshift(0);
numbers.push(numbers[numbers.length - 1] + 3);


class TreeNode {
    constructor(value, children) {
        this.value = value;
        this.children = children;
    };

    addChild(node) {
        this.children.push(child);
    }
}

let totalCombinations = 0;

// Node should be defined here
function insertInSubtree(node, numbers) {
    // console.log(process.memoryUsage().heapUsed / (1024*1024));
    if (numbers.length === 1) {
        totalCombinations++;
        return node;
    }

    for (let i = 1; i <= 3; i++) {
        if (numbers[i] - node.value == 1) {
            let newNode = new TreeNode(numbers[i], []);
            node.children.push(insertInSubtree(newNode, numbers.slice(i)));
            continue;
        }

        // 2. case checking second successor of current number
        if (numbers[i] - node.value == 2) {
            let newNode = new TreeNode(numbers[i], []);
            node.children.push(insertInSubtree(newNode, numbers.slice(i)));
            continue;
        }

        // 3. case checking third successor of current number
        if (numbers[i] - node.value == 3) {
            let newNode = new TreeNode(numbers[i], []);
            node.children.push(insertInSubtree(newNode, numbers.slice(i)));
            continue;
        }
    }

    return node;
}

function findCombinations2(curNumber) {
    if (curNumber >= numbers.length - 1) {
        totalCombinations++;
        return;
    }

    for (let i = 1; i <= 3; i++) {
        if (numbers[curNumber + i] - numbers[curNumber] == 1) {
            findCombinations2(curNumber + i);
            continue;
        }

        // 2. case checking second successor of current number
        if (numbers[curNumber + i] - numbers[curNumber] == 2) {
            findCombinations2(curNumber + i);
            continue;
        }

        // 3. case checking third successor of current number
        if (numbers[curNumber + i] - numbers[curNumber] == 3) {
            findCombinations2(curNumber + i);
            continue;
        }
    }

    // return node;
}

// Can't do it this way because only valid combination ones ending with largest number 
function findCombinations3(numbers) {
    let combinations = 1;

    let lastPossibilities = 1;
    numbers.forEach((element, index, arr) => {
        let curPossibilities = -1;
        for (let i = 1; i <= 3; i++) {
            if (arr[index + i] - element == 1) {
                curPossibilities++;
                continue;
            }

            // 2. case checking second successor of current number
            if (arr[index + i] - element == 2) {
                curPossibilities++;
                continue;
            }

            // 3. case checking third successor of current number
            if (arr[index + i] - element == 3) {
                curPossibilities++;
                continue;
            }
        }
        if (curPossibilities <= 0) {
            return;
        }
        combinations *= curPossibilities;
        // lastPossibilities = curPossibilities;
    });

    return combinations;
}

function findCombinations(node) {
    if (node.children.length === 0) {
        return 1;
    }

    let combinations = 0;
    for (let child of node.children) {
        combinations += findCombinations(child);
    }

    return combinations;
}

function tribonacciSequence(n) {
    if (n == 0) {
        return 0;
    }
    if (n == 1) {
        return 1;
    }
    if (n == 2) {
        return 1;
    }

    return tribonacciSequence(n-1) + tribonacciSequence(n-2) + tribonacciSequence(n-3);
}

function tryMe(n) {
    return 1 + (n*(n-1)/2);
}

function findCombinations4(numbers) {
    let diffs = numbers.flatMap((number, index, arr) => {
        if (index == arr.length - 1) return [];
        return [arr[index+1] - number];
    });

    let oneSeqs = [];
    let curStreak = 0
    for(let i = 0; i < diffs.length; i++) {
        if(diffs[i] === 3) {
            if(curStreak > 0) {
                oneSeqs.push(curStreak)
            }
            curStreak = 0;
            continue;
        }
        
        curStreak++;
    }

    console.log(oneSeqs);
    console.log(oneSeqs.map(tryMe).reduce((agg,cur) => agg*cur, 1));
}

findCombinations4(numbers);