const rfs = require("fs").readFileSync;

let numbers = rfs("./advent-of-code-2020-10-input-1.txt", "utf-8")
    .split("\n")
    .map((line) => Number.parseInt(line));

numbers = numbers.sort((a,b) => {if (a > b) return 1; else if (a < b) return -1; else return 0})

let diffs = {
    1: 0,
    2: 0,
    3: 1,
}

// count in the diff with plug
if(typeof diffs[numbers[0]] === 'undefined') {
    console.log("None of the chargers are compatible with socket");
    return;
}

diffs[numbers[0]] += 1;

for(let i = 0; i < numbers.length - 1; i++) {
    let difference = numbers[i+1] - numbers[i];

    diffs[difference] += 1;

    if(difference > 3) {
        console.log("Not all plugs were used");
        break;
    }
}

console.log(`One jolt diffs: ${diffs[1]}`);
console.log(`Two jolt diffs: ${diffs[2]}`);
console.log(`Three jolt diffs: ${diffs[3]}`);

console.log(diffs[1] * diffs[3]);