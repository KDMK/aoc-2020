const rfs = require("fs").readFileSync;

const data = rfs("./advent-of-code-2020-6-input-1.txt", "utf-8").split('\n');

const groupAnswers = [];
let curAnswers = {};
let totalInGroup = 0;
data.forEach(line => {
    if(line.trim().length === 0) {
        for(let key of Object.keys(curAnswers)) {
            if(curAnswers[key] !== totalInGroup) {
                delete curAnswers[key];
            } 
        }


        groupAnswers.push(curAnswers);
        curAnswers = {};
        totalInGroup = 0;
        return;
    }

    totalInGroup++;
    line.split("").forEach(character => {
        if(!curAnswers[character]) {
            curAnswers[character] = 1;
        } else {
            curAnswers[character] += 1;
        }
    });
})

let answered = groupAnswers.map(group => Object.keys(group).length);
let totalAns = answered.reduce((agg, cur) => agg+cur, 0);

console.log(answered);
console.log(totalAns);