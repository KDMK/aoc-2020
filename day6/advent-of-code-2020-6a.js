const rfs = require("fs").readFileSync;

const data = rfs("./advent-of-code-2020-6-input-1.txt", "utf-8").split('\n');

const groupAnswers = [];
let curAnswers = {};
data.forEach(line => {
    if(line.trim().length === 0) {
        groupAnswers.push(curAnswers);
        curAnswers = {};
        return;
    }

    line.split("").forEach(character => {
        curAnswers[character] = true;
    });
})

let answered = groupAnswers.map(group => Object.keys(group).length);
let totalAns = answered.reduce((agg, cur) => agg+cur, 0);

console.log(answered);
console.log(totalAns);