const rfs = require("fs").readFileSync;

const data = rfs("./advent-of-code-2020-5-input-1.txt", "utf-8").split('\n');
let seatIds = [];
let highestId = -1;

function getRow(inputCode) {
    let row = 0;
    for(let i = 0; i < 7; i++) {
        if(inputCode.charAt(i) === 'B') {
            row = row | 1;
        }
        if(i < 6) {
            row = row << 1;
        }
    }

    return row;
}

function getCol(inputCode) {
    let col = 0;
    for(let i = 0; i < 3; i++) {
        if(inputCode.charAt(i + 7) === 'R') {
            col = col | 1;
        }
        if(i < 2) {
            col = col << 1;
        }
    }

    return col;
}

function comparator(seat1, seat2) {
    if (seat1 < seat2) return -1;
    if (seat1 > seat2) return 1;
    return 0;
}

data.forEach(line => {
    let row = getRow(line);
    let col = getCol(line);

    const seatId = row * 8 + col;
    if (seatId > highestId) {
        highestId = seatId;
    }
    seatIds.push(seatId);
})

console.log(highestId);

seatIds = seatIds.sort(comparator);
console.log(seatIds);

let missingOne = seatIds.filter((curSeat, index) => {
    let nextSeat = curSeat + 1;
    return seatIds[index+1] != nextSeat && index !== seatIds.length-1;
})

console.log(missingOne[0] + 1);