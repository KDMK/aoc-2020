const rfs = require("fs").readFileSync;

const numbers = rfs("./advent-of-code-2020-9-input-1.txt", "utf-8")
    .split("\n")
    .map((line) => Number.parseInt(line));

function calculate2Combinations(numbers) {
    let combinations = {}
    for(let i = 0; i < numbers.length - 1;i++) {
        for(let j = i+1; j < numbers.length; j++) {
            combinations[numbers[i] + numbers[j]] = true;
        }
    }
    return combinations;
}

function findFaultyNumber(numbers) {
    for(let i = 25; i < numbers.length; i++) {
        let combs = calculate2Combinations(numbers.slice(i-25, i));
        if(!combs[numbers[i]]) {
            console.log(`Found faulty number ${numbers[i]} at index ${i}`);
            return numbers[i];
        }
    }
}

function findSumToFaulty(faultyNumber, numbers) {
    let sum = 0;
    let summands = [];
    for(let i = 0; i < numbers.length; i++) {
        if(numbers[i] === faultyNumber) {
            continue;
        }
        let j = i;
        while(sum < faultyNumber) {
            sum += numbers[j];
            summands.push(numbers[j]);
            if(sum === faultyNumber) {
                return summands;
            }
            j++;
        }
        sum = 0;
        summands = [];
    }
    return [];
}

let faultyNumber = findFaultyNumber(numbers);
let faultySum = findSumToFaulty(faultyNumber, numbers);

let faultySumSorted = faultySum.sort((a,b) => {if(a > b) return 1; else if (a < b) return -1; else return 0});

console.log(faultySumSorted[0] + faultySumSorted[faultySumSorted.length-1]);