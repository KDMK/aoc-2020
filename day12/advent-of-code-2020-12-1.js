const rfs = require("fs").readFileSync;

let globalDirection = "E";
let currentAngle = 0;
let globalX = 0;
let globalY = 0;

let commands = rfs("./advent-of-code-2020-12-input-1.txt", "utf-8")
    .split("\n")
    .map(line => {
        return {
            command: line.substr(0, 1),
            ammount: Number.parseInt(line.substr(1))
        }
    });

let directions = {
    N: true,
    S: true,
    W: true,
    E: true
}

let azimut = {
    "0": "E",
    "90": "N",
    "180": "W",
    "270": "S"

}

function turnRight(degrees) {
    currentAngle = (currentAngle - degrees) % 360;
    if (currentAngle < 0) {
        currentAngle = 360 + currentAngle;
    }
    globalDirection = azimut[currentAngle];
}

function turnLeft(degrees) {
    currentAngle = (currentAngle + degrees) % 360;
    if (currentAngle < 0) {
        currentAngle = 360 - currentAngle;
    }
    globalDirection = azimut[currentAngle];
}

function moveInDirection(ammount, direction) {
    switch (direction) {
        case "N":
            globalX += ammount;
            break;
        case "S":
            globalX -= ammount;
            break;
        case "E":
            globalY += ammount;
            break;
        case "W":
            globalY -= ammount;
            break;
    }
}


commands.forEach(curCommand => {
    if (directions[curCommand.command]) {
        moveInDirection(curCommand.ammount, curCommand.command);
        return;
    }
    if (curCommand.command === 'R') {
        turnRight(curCommand.ammount);
        return;
    }
    if (curCommand.command === 'L') {
        turnLeft(curCommand.ammount);
        return;
    }
    if (curCommand.command === 'F') {
        moveInDirection(curCommand.ammount, globalDirection);
    }
})

console.log("x: " + globalX);
console.log("y:" + globalY); 