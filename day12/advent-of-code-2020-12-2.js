const rfs = require("fs").readFileSync;

let globalX = 0;
let globalY = 0;

let waypointX = 1;
let waypointY = 10;

let commands = rfs("./advent-of-code-2020-12-input-1.txt", "utf-8")
    .split("\n")
    .map(line => {
        return {
            command: line.substr(0, 1),
            ammount: Number.parseInt(line.substr(1))
        }
    });

let directions = {
    N: true,
    S: true,
    W: true,
    E: true
}

let azimut = {
    "0": "E",
    "90": "N",
    "180": "W",
    "270": "S"

}

function turnRight(degrees) {
    degrees = degrees % 360;
    if (degrees == 90) {
        let third = waypointY;
        waypointY = waypointX;
        waypointX = -third;
    } else if (degrees == 180) {
        waypointX = -waypointX;
        waypointY = -waypointY;
    } else if (degrees == 270) {
        let third = waypointY;
        waypointY = -waypointX;
        waypointX = third;
    }
    console.log(`waypointX: ${waypointX}`);
    console.log(`waypointY: ${waypointY}`);
    console.log();
}

function moveShip(ammount) {
    globalX += ammount * waypointX;
    globalY += ammount * waypointY;
}

function moveWaypointInDirection(ammount, direction) {
    switch (direction) {
        case "N":
            waypointX += ammount;
            break;
        case "S":
            waypointX -= ammount;
            break;
        case "E":
            waypointY += ammount;
            break;
        case "W":
            waypointY -= ammount;
            break;
    }
}


commands.forEach(curCommand => {
    if (directions[curCommand.command]) {
        moveWaypointInDirection(curCommand.ammount, curCommand.command);
        return;
    }
    if (curCommand.command === 'R') {
        turnRight(curCommand.ammount);
        return;
    }
    if (curCommand.command === 'L') {
        turnRight(360 - (curCommand.ammount % 360));
        return;
    }
    if (curCommand.command === 'F') {
        moveShip(curCommand.ammount);
    }
})

console.log("x: " + globalX);
console.log("y:" + globalY); 