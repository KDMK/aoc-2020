const readFileSync = require("fs").readFileSync;

const data = readFileSync("./advent-of-code-2020-2-input-1.txt", "utf-8")
              .split("\n")
              .map(line => {
                  const splitted = line.split(" ");
                  const occurences = splitted[0].split("-");
                  return {
                      minOccurence: occurences[0],
                      maxOccurence: occurences[1],
                      character: splitted[1].substr(0,1),
                      password:  splitted[2]
                  }
              })

function checkPassword1(passwObj) {
    let occurences = 0;
    for(let char of passwObj.password) {
        if(char === passwObj.character) {
            occurences++;
        }
    }


    return (occurences >= passwObj.minOccurence) && (occurences <= passwObj.maxOccurence);
}

function checkPassword2(passwObj) {
    return (passwObj.password.charAt(passwObj.minOccurence-1) === passwObj.character) ^
           (passwObj.password.charAt(passwObj.maxOccurence-1) === passwObj.character)
}

const correctPasswords1 = data.filter(checkPassword1);
const correctPasswords2 = data.filter(checkPassword2);
console.log(data.length);
console.log(correctPasswords1.length);
console.log(correctPasswords2.length);